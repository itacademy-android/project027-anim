package itacademy.com.project027;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnShow).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        DialogFragmentExample dialogFragmentExample = new DialogFragmentExample();
        dialogFragmentExample.show(getSupportFragmentManager(), "example");
    }
}
