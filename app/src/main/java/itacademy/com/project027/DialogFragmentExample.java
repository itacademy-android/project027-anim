package itacademy.com.project027;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class DialogFragmentExample extends AppCompatDialogFragment {

    private EditText editText;
    private ValueAnimator focusAnimator;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_example, container, false);

        if (getDialog().getWindow() != null) {
//            getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimationStyle;
        }
        createAnimation();
        editText = view.findViewById(R.id.editText);
        editText.setOnFocusChangeListener(focusChangeListener);
        return view;
    }

    private void createAnimation() {
        int startSize = 12;
        int endSize = 28;

        focusAnimator = ValueAnimator.ofFloat(startSize, endSize);
        focusAnimator.setDuration(500);
        focusAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedValue = (float) valueAnimator.getAnimatedValue();
                editText.setTextSize(animatedValue);
            }
        });
    }

    private final View.OnFocusChangeListener focusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if (hasFocus) {
                focusAnimator.start();
            }
        }
    };
}
